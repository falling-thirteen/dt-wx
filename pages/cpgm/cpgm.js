// pages/xgxx/xgxx.js
const db=wx.cloud.database()
var app = getApp()
var address = require('../xgxx/city.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    openid:'',
    userInfo:'',
    rylblist:'',
    bmmclist:'',
    lhlist:["1号楼","2号楼","3号楼","4号楼","5号楼","6号楼","7号楼"],
    fhlist:["1111","6437"],
    xy_index:0,
    bm_index:0,
    lh_index:0,
    lh_id:'',
    fh_index:0,
    dwmclist:'',
    dw_index:0,
    fcplist:'',
    fcp_index:0,

    address: '', //详细收货地址（四级）
    value: [0, 0, 0], // 地址选择器省市区 暂存 currentIndex
    region: '', //所在地区
    regionValue: [0, 0, 0], // 地址选择器省市区 最终 currentIndex
    provinces: [], // 一级地址
    citys: [], // 二级地址
    areas: [], // 三级地址
    visible: false,
    addressId: '' // 数据库内地址编号
  },
  /**
   * 生命周期函数--监听页面加载
   */

  bindchangess:function(e){
    console.log(e)
    this.setData({
      lh_index:e.detail.value
    })
  },
  gmdj(){
    wx.navigateTo({
      url: '../fqjb/fqjb',
    })
  },
  wddd(){
    wx.navigateTo({
      url: '../wddd/wddd',
    })
  },
  onLoad: function (options) {
    const app =getApp()
    var openid =app.globalData.openid
    this.setData({
      openid:openid
    })
    // db.collection("rylb").get().then(res=>{
    //   console.log(res)
    //   this.setData({
    //     tswz:res.data
    //   })
    wx.cloud.callFunction({
      name: "rylbget",
      complete: res => {
        this.setData({
        arr:res.result.data
      })
      var arr=['请选择类型']
      for(var i=0;i<res.result.data.length;i++){
        arr.push(res.result.data[i].rylb);
      }
      this.setData({
        rylblist:arr
      })
    }
    })
    db.collection("khuser").where({openid:this.data.openid}).get().then(res=>{
      console.log(res.data)
      this.setData({
        userInfo:res.data
      })
      // console.log(this.data.userInfo[0].nj)
      // for(let i=0;i<this.data.array.length;i++){
      //   if(this,this.data.array[i]==this.data.userInfo[0].nj){
      //     console.log(i)
      //     this.setData({
      //       nj_index:i
      //     })
      //   }
      // }
      for(let i=0;i<this.data.rylblist.length;i++){
        if(this,this.data.rylblist[i]==this.data.userInfo[0].rylb){
          console.log(i)
          this.setData({
            xy_index:i
          })
        }
      }
    })
        // 默认联动显示北京
        var id = address.provinces[0].id
        this.setData({
          provinces: address.provinces,
          citys: address.citys[id],
          areas: address.areas[address.citys[id][0].id]
        })
    
  },

// 提交时由序号获取省市区id
getRegionId(type) {
  let value = this.data.regionValue
  let provinceId = address.provinces[value[0]].id
  let townId = address.citys[provinceId][value[1]].id
  let areaId
  if (address.areas.length > 0) {
    areaId = address.areas[townId][value[2]].id
  } else {
    areaId = 0
  }

  if (type === 'provinceId') {
    return provinceId
  } else if (type === 'townId') {
    return townId
  } else {
    return areaId
  }
},
closePopUp() {
  this.setData({
    visible: false
  })
},
pickAddress() {
  this.setData({
    visible: true,
    value: [...this.data.regionValue]
  })
},
// 处理省市县联动逻辑 并保存 value
cityChange(e) {
  var value = e.detail.value
  let {
    provinces,
    citys
  } = this.data
  var provinceNum = value[0]
  var cityNum = value[1]
  var areaNum = value[2]

  if (this.data.value[0] != provinceNum) {
    var id = provinces[provinceNum].id
    this.setData({
      value: [provinceNum, 0, 0],
      citys: address.citys[id],
      areas: address.areas[address.citys[id][0].id]
    })
  } else if (this.data.value[1] != cityNum) {
    var id = citys[cityNum].id
    this.setData({
      value: [provinceNum, cityNum, 0],
      areas: address.areas[citys[cityNum].id]
    })
  } else {
    this.setData({
      value: [provinceNum, cityNum, areaNum]
    })
  }
},
preventTouchmove() { },
// 城市选择器
// 点击地区选择取消按钮
cityCancel(e) {
  this.setData({
    visible: false
  })
},
// 点击地区选择确定按钮
citySure(e) {
  var value = this.data.value
  this.setData({
    visible: false
  })
  // 将选择的城市信息显示到输入框
  try {
    var region =
      (this.data.provinces[value[0]].name || '') +
      (this.data.citys[value[1]].name || '')
    if (this.data.areas.length > 0) {
      region = region + this.data.areas[value[2]].name || ''
    } else {
      this.data.value[2] = 0
    }
  } catch (error) {
    console.log('adress select something error')
  }

  this.setData({
    region: region,
    regionValue: [...this.data.value]
  })
},
  bindchangefh:function(e){
    console.log(e.detail.value)
    this.setData({
      fh_index:e.detail.value
    })
  },
  bindchangexy:function(e){
    console.log(e.detail.value)
    this.setData({
      xy_index:e.detail.value
    })
    
  },
  bindchangebm:function(e){
    console.log(e.detail.value)
    this.setData({
      bm_index:e.detail.value
    })
    
  },
  bindchangedw:function(e){
    console.log(e.detail.value)
    this.setData({
      dw_index:e.detail.value
    })
    
  },


  formSubmit(e){
    console.log(e.detail.value)
    db.collection("user").where({openid:this.data.openid}).update({
      data:{
        zccph:e.detail.value.zccph,
        username:e.detail.value.name,
        _updateTime:Date.parse(new Date()),
      },
      success:function(res){
        wx.showToast({
          title: '提交成功',
          icon: 'success',
          duration: 1500,
        })
      }
    })
  },




  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
