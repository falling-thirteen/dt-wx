// pages/xgxx/xgxx.js
const db = wx.cloud.database()
var app = getApp()
var address = require('./city.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    openid: '',
    userInfo: '',
    rylblist: '',

    lhlist: ["1号楼", "2号楼", "3号楼", "4号楼", "5号楼", "6号楼", "7号楼"],
    fhlist: ["1111", "6437"],
    xy_index: 0,
    bm_index: 0,
    bmmclist: '',
    lh_index: 0,
    lh_id: '',
    fh_index: 0,
    dwmclist: '',
    dw_index: 0,

    address: '', //详细收货地址（四级）
    value: [0, 0, 0], // 地址选择器省市区 暂存 currentIndex
    region: '', //所在地区
    regionValue: [0, 0, 0], // 地址选择器省市区 最终 currentIndex
    provinces: [], // 一级地址
    citys: [], // 二级地址
    areas: [], // 三级地址
    visible: false,
    addressId: '' // 数据库内地址编号
  },
  /**
   * 生命周期函数--监听页面加载
   */

  bindchangess: function (e) {
    console.log(e)
    this.setData({
      lh_index: e.detail.value
    })
  },
  onLoad: function (options) {
    var openid = wx.getStorageSync('openId')
    this.setData({
      openid: openid
    })
    // db.collection("rylb").get().then(res=>{
    //   console.log(res)
    //   this.setData({
    //     tswz:res.data
    //   })
    wx.cloud.callFunction({
      name: "rylbget",
      complete: res => {
        this.setData({
          arr: res.result.data
        })
        var arr = ['请选择类型']
        for (var i = 0; i < res.result.data.length; i++) {
          arr.push(res.result.data[i].rylb);
        }
        this.setData({
          rylblist: arr
        })
      }
    })

    wx.cloud.callFunction({
      name: "bumcget",
      complete: res => {
        this.setData({
          arr1: res.result.data
        })
        var arr1 = ['请选择部门']
        for (var i = 0; i < res.result.data.length; i++) {
          arr1.push(res.result.data[i].bm_mc);
        }

        this.setData({
          bmmclist: arr1
        })
      }
    })

    wx.cloud.callFunction({
      name: "sslhget",
      complete: res => {
        console.log(res.result.data)
        console.log(res.result.data[this.data.lh_index]._id)
        this.setData({
          arr3: res.result.data,
          lh_id: res.result.data[this.data.lh_index]._id
        })
        var arr3 = ['请选择楼号']
        for (var i = 0; i < res.result.data.length; i++) {
          arr3.push(res.result.data[i].ss_lh);
        }
        console.log(arr3)
        this.setData({
          lhlist: arr3
        })
      }
    })

    wx.cloud.callFunction({
      name: "ssfhget",
      complete: res => {
        this.setData({
          arr4: res.result.data
        })
        var arr4 = ['请选择房号']
        for (var i = 0; i < res.result.data.length; i++) {
          arr4.push(res.result.data[i].ss_fh);
        }
        console.log(arr4)
        this.setData({
          fhlist: arr4
        })
      }
    })
    wx.cloud.callFunction({
      name: "bumcget",
      complete: res => {
        console.log(res.result.data)
        this.setData({
          arr1: res.result.data
        })
        var arr1 = ['请选择部门']
        for (var i = 0; i < res.result.data.length; i++) {
          arr1.push(res.result.data[i].bm_mc);
        }
        console.log(arr1)
        this.setData({
          bmmclist: arr1
        })
      }
    })

    db.collection("user").where({
      _openid: this.data.openid
    }).get().then(res => {
      let data = res.data[0]
      data.role_type = data.role_type ? data.role_type : 0
      data.dept = data.dept ? data.dept : 0
      data.comp = data.comp ? data.comp : 0
      data.build_no = data.build_no ? data.build_no : 0
      data.dom_no = data.dom_no ? data.dom_no : 0
      this.setData({
        userInfo: data
      })
      // console.log(this.data.userInfo[0].nj)
      // for(let i=0;i<this.data.array.length;i++){
      //   if(this,this.data.array[i]==this.data.userInfo[0].nj){
      //     console.log(i)
      //     this.setData({
      //       nj_index:i
      //     })
      //   }
      // }
      for (let i = 0; i < this.data.rylblist.length; i++) {
        if (this, this.data.rylblist[i] == this.data.userInfo.role_name) {
          console.log(i)
          let temp = this.data.userInfo
          temp.role_type = i
          this.setData({
            userInfo:temp
          })
        }
      }
      for (let i = 0; i < this.data.bmmclist.length; i++) {
        if (this, this.data.bmmclist[i] == this.data.userInfo.dept_name) {
          let temp = this.data.userInfo
          temp.dept_name = i
          this.setData({
            userInfo:temp
          })
        }
      }
      for (let i = 0; i < this.data.lhlist.length; i++) {
        if (this, this.data.lhlist[i] == this.data.userInfo.build_name) {
          let temp = this.data.userInfo
          temp.build_name = i
          this.setData({
            userInfo:temp
          })
        }
      }
      for (let i = 0; i < this.data.fhlist.length; i++) {
        if (this, this.data.fhlist[i] == this.data.userInfo.dom_name) {
          let temp = this.data.userInfo
          temp.dom_name = i
          this.setData({
            userInfo:temp
          })
        }
      }
    })
    // 默认联动显示北京
    var id = address.provinces[0].id
    this.setData({
      provinces: address.provinces,
      citys: address.citys[id],
      areas: address.areas[address.citys[id][0].id]
    })

  },

  // 提交时由序号获取省市区id
  getRegionId(type) {
    let value = this.data.regionValue
    let provinceId = address.provinces[value[0]].id
    let townId = address.citys[provinceId][value[1]].id
    let areaId
    if (address.areas.length > 0) {
      areaId = address.areas[townId][value[2]].id
    } else {
      areaId = 0
    }

    if (type === 'provinceId') {
      return provinceId
    } else if (type === 'townId') {
      return townId
    } else {
      return areaId
    }
  },
  closePopUp() {
    this.setData({
      visible: false
    })
  },
  pickAddress() {
    this.setData({
      visible: true,
      value: [...this.data.regionValue]
    })
  },
  // 处理省市县联动逻辑 并保存 value
  cityChange(e) {
    var value = e.detail.value
    let {
      provinces,
      citys
    } = this.data
    var provinceNum = value[0]
    var cityNum = value[1]
    var areaNum = value[2]

    if (this.data.value[0] != provinceNum) {
      var id = provinces[provinceNum].id
      this.setData({
        value: [provinceNum, 0, 0],
        citys: address.citys[id],
        areas: address.areas[address.citys[id][0].id]
      })
    } else if (this.data.value[1] != cityNum) {
      var id = citys[cityNum].id
      this.setData({
        value: [provinceNum, cityNum, 0],
        areas: address.areas[citys[cityNum].id]
      })
    } else {
      this.setData({
        value: [provinceNum, cityNum, areaNum]
      })
    }
  },
  preventTouchmove() {},
  // 城市选择器
  // 点击地区选择取消按钮
  cityCancel(e) {
    this.setData({
      visible: false
    })
  },
  // 点击地区选择确定按钮
  citySure(e) {
    var value = this.data.value
    this.setData({
      visible: false
    })
    // 将选择的城市信息显示到输入框
    try {
      var region =
        (this.data.provinces[value[0]].name || '') +
        (this.data.citys[value[1]].name || '')
      if (this.data.areas.length > 0) {
        region = region + this.data.areas[value[2]].name || ''
      } else {
        this.data.value[2] = 0
      }
    } catch (error) {
      console.log('adress select something error')
    }
    let temp = this.data.userInfo
    temp.region = region
    this.setData({
      userInfo: temp,
      regionValue: [...this.data.value]
    })
  },
  bindchangess:function(e){
    let temp = this.data.userInfo
    temp.build_no = e.detail.value
    this.setData({
      userInfo: temp
    })
  },
  bindchangefh: function (e) {
    console.log(e.detail.value)
    let temp = this.data.userInfo
    temp.dom_no = e.detail.value
    this.setData({
      userInfo: temp
    })
  },
  bindchangexy: function (e) {
    console.log(e.detail.value)
    let temp = this.data.userInfo
    console.log(temp)
    temp.role_type = e.detail.value
    this.setData({
      userInfo: temp
    })

  },
  bindchangebm: function (e) {
    console.log(e.detail.value)
    let temp = this.data.userInfo
    temp.dept = e.detail.value
    this.setData({
      userInfo: temp
    })

  },
  bindchangedw: function (e) {
    console.log(e.detail.value)
    let temp = this.data.userInfo
    temp.comp = e.detail.value
    this.setData({
      userInfo: temp
    })

  },
  inputName(e){
    let temp = this.data.userInfo
    temp.name = e.detail.value
    this.setData({
      userInfo:temp
    })
  },
  inputPhone(e){
    let temp = this.data.userInfo
    temp.phone = e.detail.value
    this.setData({
      userInfo:temp
    })
  },
  inputId(e){
    let temp = this.data.userInfo
    temp.id_num = e.detail.value
    this.setData({
      userInfo:temp
    })
  },

  formSubmit(e) {
    let temp = this.data.userInfo
    temp._updateTime = Date.parse(new Date())
    db.collection("user").doc(temp._id).update({
      data: {
        phone:temp.phone,
        build_no: Number(temp.build_no),
        build_name:this.data.lhlist[temp.build_no],
        comp: Number(temp.comp),
        dept: Number(temp.dept),
        dept_name: this.data.bmmclist[temp.dept],
        dom_no: Number(temp.dom_no),
        dom_name:this.data.fhlist[temp.dom_no],
        role_type: Number(temp.role_type),
        role_name:this.data.rylblist[temp.role_type],
        name:temp.name,
        region:temp.region,
        id_num:temp.id_num,
        _updateTime: Date.parse(new Date()),
      },
      success: function (res) {
        wx.navigateTo({
          url: '../grzl/grzl',
        })
      },
      error:function(res){
        console.log(res)
      }
    })
  },





  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})