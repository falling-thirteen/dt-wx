// pages/fbxx/fbxx.js
const db = wx.cloud.database()
var app = getApp()
var address = require('../xgxx/city.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fcplist:["焦油0号","焦油1号","焦油2号","粗酚","硫磺","硫酸铵","液氧","液氮","氨水","粉煤灰","液氧","炉渣","LNG"],
    fcp_index:0,
    fbxx:'',
    formData: {
      fcpmc:"",
      zccph:"",
      gccph:"",
      sjname:"",
      sjsfz:"",
      sjcyz:"",
      shouji:"",
      ysgsmc:"",
      dljyxkz:"",
      yyyname:"",
      yyycyzh:"",
      file1:[],
      file2:[],
      file3:[],
      file4:[],
      file5:[],
      file6:[]
    },
    date:'',
    time:'',
    openid:'',
    files: [],
    fblx:''
  },
  
  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);
    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }
    this.setData({
      radioItems: radioItems,
      fblx: this.data.radioItems[e.detail.value].name
    });
  },
  
  onLoad: function (options) {
    const app =getApp()
    var openid =app.globalData.openid
    this.setData({
      openid:openid
    })

    db.collection("user").where({openid:this.data.openid}).get().then(res=>{
      console.log(res.data)
      this.setData({
        userInfo:res.data
      })
      // console.log(this.data.userInfo[0].nj)
      // for(let i=0;i<this.data.array.length;i++){
      //   if(this,this.data.array[i]==this.data.userInfo[0].nj){
      //     console.log(i)
      //     this.setData({
      //       nj_index:i
      //     })
      //   }
      // }
      for(let i=0;i<this.data.fcplist.length;i++){
        if(this,this.data.fcplist[i]==this.data.userInfo[0].rylb){
          console.log(i)
          this.setData({
            xy_index:i
          })
        }
      }
    })
  
  },
  bindchangefcp:function(e){
    console.log(e.detail.value)
    this.setData({
      fcp_index:e.detail.value
    })
    
  },
  submitForm(e){
    console.log(e)
    if(e.detail.value.zccph === "" || e.detail.value.gccph === "" || e.detail.value.sjname === "" || e.detail.value.sjsfz === "" || e.detail.value.sjcyz === "" || e.detail.value.shouji === "" || e.detail.value.ysgsmc === "" || e.detail.value.dljyxkz === "" || e.detail.value.yyyname === "" || e.detail.value.yyycyzh === ""){
      wx.showToast({
        title: '表单信息不完整！',
        icon:'error'
      })
    }
    else if(this.data.formData.file1.length < 6 || this.data.formData.file2.length < 6 || this.data.formData.file3.length < 3 || this.data.formData.file4.length < 1 || this.data.formData.file5.length < 4 || this.data.formData.file6.length < 4){
      wx.showToast({
        title: '附件数量不足！',
        icon:'error'
      })
    }
    else{
      db.collection('swzl').add({
        data: {
          openid: this.data.openid,
          fcpmc:this.data.fcplist[this.data.fcp_index],
          zccph:e.detail.value.zccph,
          gccph:e.detail.value.gccph,
          sjname:e.detail.value.sjname,
          sjsfz:e.detail.value.sjsfz,
          sjcyz:e.detail.value.sjcyz,
          shouji:e.detail.value.shouji,
          ysgsmc:e.detail.value.ysgsmc,
          dljyxkz:e.detail.value.dljyxkz,
          yyyname:e.detail.value.yyyname,
          yyycyzh:e.detail.value.yyycyzh,
          file1: this.data.formData.file1,
          file2: this.data.formData.file2,
          file3: this.data.formData.file3,
          file4: this.data.formData.file4,
          file5: this.data.formData.file5,
          file6: this.data.formData.file6,
          data:e.detail.value.date,
          _createTime: Date.parse(new Date()),
          status:0
        }
      }).then(res => {
        wx.showToast({
          title: '提交成功',
          icon: 'success',
          duration: 2000,
          success:res=>{
            setTimeout(function(){
              wx.switchTab({
                url: '../my/my',
              })
            },2000)
          }
        })
      })
    }
  },
  formip(e){
    console.log(e.detail.value)
    this.setData({
      fbxx: e.detail.value
    })
  },
  bindDateChange:function(e){
    this.setData({
      date:e.detail.value
    })
  },
  bindtimeChange:function(e){
    this.setData({
      time:e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const app=getApp()
    var openid=app.globalData.openid
    if(options.id){
      db.collection("swzl").doc(options.id).get().then(res => {
        console.log(res)
        this.setData({
          formData:res.data
        })
      })
    }
    this.setData({
      openid:openid
    })
    this.setData({
      selectFile: this.selectFile.bind(this),
      uplaodFile1: this.uplaodFile1.bind(this),
      uplaodFile2: this.uplaodFile2.bind(this),
      uplaodFile3: this.uplaodFile3.bind(this),
      uplaodFile4: this.uplaodFile4.bind(this),
      uplaodFile5: this.uplaodFile5.bind(this),
      uplaodFile6: this.uplaodFile6.bind(this),
    })
  },
  chooseImage: function (e) {
    var that = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          files: that.data.files.concat(res.tempFilePaths)
        });
      }
    })
  },
  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },
  selectFile(files) {
    console.log('files', files)
    // 返回false可以阻止某次文件上传
  },
  uplaodFile1(files) {
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
      const tempFilePaths = files.tempFilePaths;
      //上传返回值
      const that = this;
      let object = {};
      for (var i = 0; i < tempFilePaths.length; i++) {
        let filePath = '', cloudPath = ''
        filePath = tempFilePaths[i]
        // 上传图片
        // cloudPath 最好按时间 遍历的index来排序，避免文件名重复
        cloudPath = 'blog-title-image-' + new Date().getTime() + '-' + i + filePath.match(/\.[^.]+?$/)[0]
        const upload_task = wx.cloud.uploadFile({
          filePath,
          cloudPath,
          success: function (res) {
            console.log(res)
            // 可能会有好几个200+的返回码，表示成功
            if (res.statusCode === 200 || res.statusCode === 204 || res.statusCode === 205) {
              const url = res.fileID
              that.data.formData.file1.push(url)
              if (that.data.formData.file1.length === tempFilePaths.length) {
                object.urls = that.data.formData.file1;
                resolve(object)  //这就是判断是不是最后一张已经上传了，用来返回，
              }
            }
          },
          fail: function (err) {
            reject('error')
          },
          conplete: () => {
              
          }
        })
      }
    })
    // 文件上传的函数，返回一个promise
  },
  uplaodFile2(files) {
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
      const tempFilePaths = files.tempFilePaths;
      //上传返回值
      const that = this;
      let object = {};
      for (var i = 0; i < tempFilePaths.length; i++) {
        let filePath = '', cloudPath = ''
        filePath = tempFilePaths[i]
        // 上传图片
        // cloudPath 最好按时间 遍历的index来排序，避免文件名重复
        cloudPath = 'blog-title-image-' + new Date().getTime() + '-' + i + filePath.match(/\.[^.]+?$/)[0]
        const upload_task = wx.cloud.uploadFile({
          filePath,
          cloudPath,
          success: function (res) {
            console.log(res)
            // 可能会有好几个200+的返回码，表示成功
            if (res.statusCode === 200 || res.statusCode === 204 || res.statusCode === 205) {
              const url = res.fileID
              that.data.formData.file2.push(url)
              if (that.data.formData.file2.length === tempFilePaths.length) {
                object.urls = that.data.formData.file2;
                resolve(object)  //这就是判断是不是最后一张已经上传了，用来返回，
              }
            }
          },
          fail: function (err) {
            reject('error')
          },
          conplete: () => {
              
          }
        })
      }
    })
    // 文件上传的函数，返回一个promise
  },
  uplaodFile3(files) {
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
      const tempFilePaths = files.tempFilePaths;
      //上传返回值
      const that = this;
      let object = {};
      for (var i = 0; i < tempFilePaths.length; i++) {
        let filePath = '', cloudPath = ''
        filePath = tempFilePaths[i]
        // 上传图片
        // cloudPath 最好按时间 遍历的index来排序，避免文件名重复
        cloudPath = 'blog-title-image-' + new Date().getTime() + '-' + i + filePath.match(/\.[^.]+?$/)[0]
        const upload_task = wx.cloud.uploadFile({
          filePath,
          cloudPath,
          success: function (res) {
            console.log(res)
            // 可能会有好几个200+的返回码，表示成功
            if (res.statusCode === 200 || res.statusCode === 204 || res.statusCode === 205) {
              const url = res.fileID
              that.data.formData.file3.push(url)
              if (that.data.formData.file3.length === tempFilePaths.length) {
                object.urls = that.data.formData.file3;
                resolve(object)  //这就是判断是不是最后一张已经上传了，用来返回，
              }
            }
          },
          fail: function (err) {
            reject('error')
          },
          conplete: () => {
              
          }
        })
      }
    })
    // 文件上传的函数，返回一个promise
  },
  uplaodFile4(files) {
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
      const tempFilePaths = files.tempFilePaths;
      //上传返回值
      const that = this;
      let object = {};
      for (var i = 0; i < tempFilePaths.length; i++) {
        let filePath = '', cloudPath = ''
        filePath = tempFilePaths[i]
        // 上传图片
        // cloudPath 最好按时间 遍历的index来排序，避免文件名重复
        cloudPath = 'blog-title-image-' + new Date().getTime() + '-' + i + filePath.match(/\.[^.]+?$/)[0]
        const upload_task = wx.cloud.uploadFile({
          filePath,
          cloudPath,
          success: function (res) {
            console.log(res)
            // 可能会有好几个200+的返回码，表示成功
            if (res.statusCode === 200 || res.statusCode === 204 || res.statusCode === 205) {
              const url = res.fileID
              that.data.formData.file4.push(url)
              if (that.data.formData.file4.length === tempFilePaths.length) {
                object.urls = that.data.formData.file4;
                resolve(object)  //这就是判断是不是最后一张已经上传了，用来返回，
              }
            }
          },
          fail: function (err) {
            reject('error')
          },
          conplete: () => {
              
          }
        })
      }
    })
    // 文件上传的函数，返回一个promise
  },
  uplaodFile5(files) {
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
      const tempFilePaths = files.tempFilePaths;
      //上传返回值
      const that = this;
      let object = {};
      for (var i = 0; i < tempFilePaths.length; i++) {
        let filePath = '', cloudPath = ''
        filePath = tempFilePaths[i]
        // 上传图片
        // cloudPath 最好按时间 遍历的index来排序，避免文件名重复
        cloudPath = 'blog-title-image-' + new Date().getTime() + '-' + i + filePath.match(/\.[^.]+?$/)[0]
        const upload_task = wx.cloud.uploadFile({
          filePath,
          cloudPath,
          success: function (res) {
            console.log(res)
            // 可能会有好几个200+的返回码，表示成功
            if (res.statusCode === 200 || res.statusCode === 204 || res.statusCode === 205) {
              const url = res.fileID
              that.data.formData.file5.push(url)
              if (that.data.formData.file5.length === tempFilePaths.length) {
                object.urls = that.data.formData.file5;
                resolve(object)  //这就是判断是不是最后一张已经上传了，用来返回，
              }
            }
          },
          fail: function (err) {
            reject('error')
          },
          conplete: () => {
              
          }
        })
      }
    })
    // 文件上传的函数，返回一个promise
  },
  uplaodFile6(files) {
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
      const tempFilePaths = files.tempFilePaths;
      //上传返回值
      const that = this;
      let object = {};
      for (var i = 0; i < tempFilePaths.length; i++) {
        let filePath = '', cloudPath = ''
        filePath = tempFilePaths[i]
        // 上传图片
        // cloudPath 最好按时间 遍历的index来排序，避免文件名重复
        cloudPath = 'blog-title-image-' + new Date().getTime() + '-' + i + filePath.match(/\.[^.]+?$/)[0]
        const upload_task = wx.cloud.uploadFile({
          filePath,
          cloudPath,
          success: function (res) {
            console.log(res)
            // 可能会有好几个200+的返回码，表示成功
            if (res.statusCode === 200 || res.statusCode === 204 || res.statusCode === 205) {
              const url = res.fileID
              that.data.formData.file6.push(url)
              if (that.data.formData.file6.length === tempFilePaths.length) {
                object.urls = that.data.formData.file6;
                resolve(object)  //这就是判断是不是最后一张已经上传了，用来返回，
              }
            }
          },
          fail: function (err) {
            reject('error')
          },
          conplete: () => {
              
          }
        })
      }
    })
    // 文件上传的函数，返回一个promise
  },
  uploadError(e) {
    wx.hideLoading()
  },
  uploadSuccess(e) {
    console.log('upload success', this.data.files)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})