// pages/my/my.js
const app = getApp();
const db = wx.cloud.database()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    openid: '',
    userInfo: '',
    isLogin: false
  },
  getopenid() {
    var that = this;
    console.log(this.data.openid);
    wx.showLoading({
      title:"登陆中"
    })
    wx.cloud.callFunction({
          name: 'open',
          success: (res) => {
            var usid = res.result.openid
            wx.setStorageSync('openId', usid)
            this.setData({
              openid: res.result.openid,
            })
            db.collection("user").where({
                _openid: this.data.openid
              }).get().then(res => {
                  this.setData({
                    userInfo: res.data,
                    isLogin : true
                  })
                  if (res.data == '') {
                    wx.navigateTo({
                      url: '../getuser/getuser',
                    })
                  }
                  else{
                    wx.showToast({
                      title: '登录成功',
                      icon:'success',
                      duration:2000
                    })
                  }
                  wx.setStorageSync('userinfo', res.data)
                })
                wx.hideLoading()
              },
              fail(res) {
                console.log("获取失败", res);
                wx.hideLoading()
              }
          })
        // wx.getUserProfile({
        //   desc:'用于完善资料',//声明
        //   success:(res)=>{
        //     this.setData({
        //       userInfo:res.userInfo
        //     })
        //   },
        //   fail(res){
        //     console.log('用户拒绝')
        //   }
        // })
      },
      cpgm() {
        if(this.data.isLogin){
          wx.navigateTo({
            url: '../fqjb/fqjb',
          })
        }
        else{
          wx.showToast({
            title: '请登录',
            icon: 'error',
            duration: 2000
          })
        } 
      },
      grzl() {
        if (this.data.openid != '') {
          wx.navigateTo({
            url: '../grzl/grzl',
          })
        } else {
          wx.showToast({
            title: '请登录',
            icon: 'error',
            duration: 2000
          })
        }
      },
      /**
       * 生命周期函数--监听页面加载
       */
      onLoad: function (options) {


      },
      showgyqfk() {
        wx.navigateTo({
          url: '../yjfk/yjfk',
        })
      },
      fwzx() {
        if (this.data.openid != '') {
          wx.navigateTo({
            url: '../myOrder/myOrder',
          })
        } else {
          wx.showToast({
            title: '请登录',
            icon: 'error',
            duration: 2000
          })
        }
      },
      showgywm() {
        wx.navigateTo({
          url: '../gywm/gywm',
        })
      },
      showgl() {
        if (this.data.userInfo[0].isadmin == 1) {
          wx.navigateTo({
            url: '../admin/admin',
          })
        } else {
          wx.showToast({
            title: '无权限',
            icon: 'error',
            duration: 2000
          })
        }
      },
      /**
       * 生命周期函数--监听页面初次渲染完成
       */
      onReady: function () {

      },

      /**
       * 生命周期函数--监听页面显示
       */
      onShow: function () {
        // let user=wx.getStorageSync('openid')
        // var userinfo =wx.getStorageSync('userinfo')
        // this.setData({
        //   openid:user,
        //   userInfo:userinfo
        // })
        // getApp().globalData.openid = user
        // if(this.data.userInfo==''){
        // db.collection("user").where({ openid: user }).get().then(res => {
        //   console.log(res.data)
        //   this.setData({
        //     userInfo: res.data
        //   })
        //   wx.setStorageSync('userinfo', res.data)
        // })
      }
  }
)