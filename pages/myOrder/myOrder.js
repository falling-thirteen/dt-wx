// pages/myOrder.js
const app = getApp()
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    test: "https://res.wx.qq.com/wxdoc/dist/assets/img/0.4cb08bb4.jpg",
    order_list: [],
    isAdmin: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.type) {
      this.setData({
        isAdmin: true
      })
    }
    if (this.data.isAdmin) {
      db.collection('swzl').get().then(res => {
        this.setData({
          order_list: res.data
        })
      })
    } else {
      let openId = wx.getStorageSync('openId')
      db.collection("swzl").where({
        "_openid": openId
      }).get().then(res => {
        this.setData({
          order_list: res.data
        })
      })
    }

  },
  goDetail(e) {
    let item = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '../orderDetail/orderDetail?id=' + item._id,
    })
  },
  qrCode(e) {
    let item = e.currentTarget.dataset.item
    if (this.data.isAdmin) {
      if (item.status !== 0) {
        wx.showToast({
          title: '该订单已处理!',
          icon: 'error'
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '确定驳回该订单吗？',
          success: res => {
            if (res.confirm) {
              db.collection('swzl').doc(item._id).update({
                data: {
                  status: -1
                },
                success: res => {
                  wx.showToast({
                    title: '驳回成功!',
                    icon: 'success'
                  })
                }
              })
            } else {
              console.log("取消")
            }
          }
        })
      }

    } else {
      if (item.status == 1) {
        wx.navigateTo({
          url: '../qrCode/qrCode?id=' + item._id,
        })
      } else if(item.status == 0){
        wx.showToast({
          title: '订单还未审核!',
          icon: 'error'
        })
      }
      else if(item.status == 2){
        wx.showToast({
          title: '该订单已完成!',
          icon: 'error'
        })
      }
      else{
        wx.showToast({
          title: '请取消该订单!',
          icon: 'error'
        })
      }
    }
  },
  cancelOrder(e) {
    let item = e.currentTarget.dataset.item
    if (this.data.isAdmin) {
      if (item.status !== 0) {
        wx.showToast({
          title: '该订单已处理!',
          icon: 'error'
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '确定审批该订单吗？',
          success: res => {
            if (res.confirm) {
              db.collection('swzl').doc(item._id).update({
                data: {
                  status: 1
                },
                success: res => {
                  wx.showToast({
                    title: '审批成功!',
                    icon: 'success'
                  })
                }
              })
            } else {
              console.log("取消")
            }
          }
        })
      }
    } else {
      if (item.status == 2) {
        wx.showToast({
          title: '该订单已完成!',
          icon: 'error'
        })
      }
      else if(item.status == 1){
        wx.showToast({
          title: '该订单已审核!',
          icon: 'error'
        })
      } 
      else {
        wx.showModal({
          title: '提示',
          content: '确定要取消该预约订单吗？',
          success: res => {
            if (res.confirm) {
              db.collection("swzl").doc(item._id).remove({
                success: res => {
                  wx.showToast({
                    title: '取消成功!',
                    icon:'success'
                  })
                }
              })
            } else {
              console.log("取消")
            }
          }
        })
      }
    }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})