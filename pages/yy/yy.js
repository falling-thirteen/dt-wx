// pages/zwyy/zwyy.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: 0,
    placearr: ['查看全部', '新图一楼', '新图二楼'],
    ztarr: ['查看全部','可预约', '暂未预约', '不可预约'],
    yyzt:0,
    rmb:'',
    tswz:''
  },
  bindPickerChangeplace: function (e) {
    console.log(e.detail.value)
    this.setData({
      index: e.detail.value,
    })
  },
  bindPickerChangezt:function (e) {
    console.log(e.detail.value)
    this.setData({
      yyzt: e.detail.value,
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // db.collection("tsgzw").get().then(res=>{
    //   console.log(res)
    //   this.setData({
    //     rmb:res.data
    //   })
    // })
    wx.cloud.callFunction({
      name: "getzw",
      complete: res => {
        console.log(res.result.data)
        this.setData({
        rmb:res.result.data
      })
      }
    })
    db.collection("tsgwz").get().then(res=>{
      console.log(res)
      this.setData({
        tswz:res.data
      })
      var arr=['查看全部']
      for(var i=0;i<res.data.length;i++){
        arr.push(res.data[i].tsglc);
      }
      console.log(arr)
      this.setData({
        placearr:arr
      })
    })
      
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
 
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})