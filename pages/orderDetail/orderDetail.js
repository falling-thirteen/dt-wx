// pages/orderDetail/orderDetail.js
const db = wx.cloud.database()
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    formData:"",
    isAdmin:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.type){
      this.setData({
        isAdmin:true
      })
    }
    db.collection("swzl").doc(options.id).get().then(res => {
      console.log(res)
      res.data._createTime = this.dateUtil(res.data._createTime)
      this.setData({
        formData:res.data
      })
    })
  },
  completeOrder(){
    db.collection("swzl").doc(this.data.formData._id).update({
      data:{
        status:2
      },
      success: res=>{
        wx.showToast({
          title: '订单已完成',
          icon:'success',
          duration:1000,
          complete:( )=> {
            wx.navigateTo({
              url: '../admin/admin',
            })
          }
        })
      }
    })
  },
  goDetail(){
    wx.navigateTo({
      url: '../fileDetail/fileDetail?id=' + this.data.formData._id,
    })
  },
  dateUtil(time){
    let day = new Date(time)
    let res = `${day.getFullYear()}-${day.getMonth()}-${day.getDate()} ${day.getHours()}:${day.getMinutes()}:${day.getSeconds()}`
    return res
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})